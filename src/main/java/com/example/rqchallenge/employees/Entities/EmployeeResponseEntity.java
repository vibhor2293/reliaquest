package com.example.rqchallenge.employees.Entities;

import java.util.List;

public class EmployeeResponseEntity {

    private String status;
    private List<Employee> data;


    public String getStatus() {
        return status;
    }

    public List<Employee> getData() {
        return data;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setData(List<Employee> data) {
        this.data = data;
    }
}

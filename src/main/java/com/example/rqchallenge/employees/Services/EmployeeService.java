package com.example.rqchallenge.employees.Services;

import com.example.rqchallenge.employees.Entities.ApiResponse;
import com.example.rqchallenge.employees.Entities.Employee;
import com.example.rqchallenge.employees.Entities.EmployeeResponseEntity;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class EmployeeService {


    @Value("${external.api.baseurl}")
    private String apiUrl;

    private final RestTemplate restTemplate;
    @Autowired

    public EmployeeService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
        public List<Employee> getAllEmployees() {
            String url = apiUrl + "/employees";
            ResponseEntity<ApiResponse<List<Employee>>> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<ApiResponse<List<Employee>>>() {}
            );

            return responseEntity.getBody().getData();
        }

    public List<Employee> getEmployeesByNameSearch(String searchString) {
        String url = apiUrl + "/employees";
        ResponseEntity<ApiResponse<List<Employee>>> responseEntity = restTemplate.exchange(
                url,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<ApiResponse<List<Employee>>>() {}
        );

        List<Employee> allEmployees = responseEntity.getBody().getData();

        return allEmployees.stream()
                .filter(employee -> employee.getEmployeeName().contains(searchString))
                .collect(Collectors.toList());
    }

    public Employee getEmployeeById(String id) {
        String url = apiUrl + "/employee/" + id;
        ResponseEntity<ApiResponse<Employee>> responseEntity = restTemplate.exchange(
                url,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<ApiResponse<Employee>>() {}
        );

        return responseEntity.getBody().getData();
    }



    public Integer getHighestSalaryOfEmployees() {
        String url = apiUrl + "/employees";
        ResponseEntity<ApiResponse<List<Employee>>> responseEntity =
                restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<ApiResponse<List<Employee>>>() {});

        List<Employee> allEmployees = responseEntity.getBody().getData();

        return allEmployees.stream()
                .map(Employee::getEmployeeSalary)
                .map(Integer::parseInt)
                .max(Integer::compareTo)
                .orElse(0);
    }

    public List<Employee> getTopTenHighestEarningEmployeeNames() {
        String url = apiUrl + "/employees";
        ResponseEntity<ApiResponse<List<Employee>>> responseEntity =
                restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<ApiResponse<List<Employee>>>() {});

        List<Employee> allEmployees = responseEntity.getBody().getData();

        return allEmployees.stream()
                .sorted((e1, e2) -> Integer.compare(
                        Integer.parseInt(e2.getEmployeeSalary()),
                        Integer.parseInt(e1.getEmployeeSalary())))
                .limit(10)
                .collect(Collectors.toList());
    }

    public Employee createEmployee(Map<String, Object> employeeInput) {
        String url = apiUrl + "/create";

        try {
            String jsonInput = convertMapToJson(employeeInput);

            // Assuming the keys in the employeeInput map are "name", "salary", and "age"
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> requestEntity = new HttpEntity<>(jsonInput, headers);

            ResponseEntity<ApiResponse<Employee>> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    requestEntity,
                    new ParameterizedTypeReference<ApiResponse<Employee>>() {}
            );

            return responseEntity.getBody().getData();
        } catch (IOException e) {
            // Handle exception
            e.printStackTrace();
            return null;
        }
    }

    public String deleteEmployeeById(String id) {
        String url = apiUrl + "/delete/" + id;
        ResponseEntity<ApiResponse<String>> responseEntity =
                restTemplate.exchange(url, HttpMethod.DELETE, null, new ParameterizedTypeReference<ApiResponse<String>>() {});

        return responseEntity.getBody().getMessage();
    }

    public static String convertMapToJson(Map<String, Object> data) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(data);
    }
}
